-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2021 at 07:54 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inv`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `date_time` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `date_time`, `description`, `user_id`) VALUES
(1, '2021-03-02 09:43:32', 'Logout - \'admin\'', 1),
(2, '2021-03-02 09:43:35', 'Login - \'admin\'', 1),
(3, '2021-03-02 11:04:34', 'Login - \'admin\'', 1),
(4, '2021-03-02 11:20:44', 'User - password changed : Admin', 1),
(5, '2021-03-02 11:22:27', 'Category saved - Demo', 1),
(6, '2021-03-02 11:23:15', 'Product - saved : Demo Product', 1),
(7, '2021-03-02 11:25:08', 'Category saved - Demo', 1),
(8, '2021-03-02 16:40:18', 'Logout - \'admin\'', 1),
(9, '2021-03-02 16:40:28', 'Login - \'admin\'', 1),
(10, '2021-03-03 09:26:10', 'Login - \'admin\'', 1),
(11, '2021-03-03 10:31:23', 'Logout - \'admin\'', 1),
(12, '2021-03-03 10:31:26', 'Login - \'admin\'', 1),
(13, '2021-03-03 10:40:34', 'Role - saved : Department', 1),
(14, '2021-03-03 10:41:00', 'Role - deleted : Department', 1),
(15, '2021-03-03 11:03:48', 'Privilege - updated : for SUPER ADMIN', 1),
(16, '2021-03-03 11:27:57', 'Department saved - Demo Department', 1),
(17, '2021-03-03 11:28:37', 'Department updated - Demo Departmentt', 1),
(18, '2021-03-03 11:28:55', 'Department deleted - Demo Departmentt', 1),
(19, '2021-03-03 11:30:17', 'Department saved - Demo Department', 1),
(20, '2021-03-03 14:24:25', 'Logout - \'admin\'', 1),
(21, '2021-03-03 14:24:28', 'Login - \'admin\'', 1),
(22, '2021-03-03 17:58:12', 'Logout - \'admin\'', 1),
(23, '2021-03-03 17:58:15', 'Login - \'admin\'', 1),
(24, '2021-03-03 18:52:08', 'Customer - saved:aa', 1),
(25, '2021-03-04 08:25:22', 'Logout - \'admin\'', 1),
(26, '2021-03-04 08:25:27', 'Login - \'admin\'', 1),
(27, '2021-03-04 09:13:52', 'Supplier - saved : Dananjaya', 1),
(28, '2021-03-04 09:16:06', 'Supplier - updated : Dananjayaa', 1),
(29, '2021-03-04 09:25:15', 'Supplier - deleted : Dananjayaa', 1),
(30, '2021-03-04 10:45:12', 'Logout - \'admin\'', 1),
(31, '2021-03-04 10:45:17', 'Login - \'admin\'', 1),
(32, '2021-03-04 11:35:18', 'Product - saved : demoo', 1),
(33, '2021-03-04 11:36:53', 'Product - saved : demoo', 1),
(34, '2021-03-04 11:38:01', 'Product - saved : demoo', 1),
(35, '2021-03-04 11:38:39', 'Product - saved : demoo', 1),
(36, '2021-03-04 11:39:28', 'Product - saved : demoo', 1),
(37, '2021-03-04 12:06:40', 'Product - updated : demoo', 1),
(38, '2021-03-04 12:08:39', 'Product - updated : demoo', 1),
(39, '2021-03-04 12:14:38', 'Product - updated : demoo', 1),
(40, '2021-03-04 12:17:45', 'Product - updated : demoo', 1),
(41, '2021-03-04 12:19:04', 'Product - updated : demoo', 1),
(42, '2021-03-04 12:20:34', 'Product - updated : demoo', 1),
(43, '2021-03-04 12:21:14', 'Product - updated : demoo', 1),
(44, '2021-03-04 12:22:06', 'Product - deleted : Demo Product', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `name`) VALUES
(1, 'Sampath Bank'),
(2, 'Commercial Bank'),
(3, 'HNB Bank'),
(4, 'NSB Bank'),
(5, 'BOC Bank'),
(6, 'Amana Bank'),
(7, 'Axis Bank'),
(8, 'Cargills Bank'),
(9, 'Citibank N.A.'),
(10, 'Deutsche Bank AG'),
(11, 'DFCC Bank'),
(12, 'Habib Bank'),
(13, 'ICICI Bank'),
(14, 'Indian Bank'),
(15, 'Indian Overseas Bank'),
(16, 'MCB Bank Ltd'),
(17, 'National Development Bank'),
(18, 'Nations Trust Bank'),
(19, 'Pan Asia Banking Corporation'),
(20, 'People\'s Bank'),
(21, 'Public Bank Berhad'),
(22, 'Seylan Bank'),
(23, 'Standard Chartered Bank'),
(24, 'State Bank of India'),
(25, 'HSBC '),
(26, 'Union Bank'),
(27, 'HDFC '),
(28, 'Lankaputhra Development Bank'),
(29, 'RDB (Regional Development)'),
(30, 'SDB (Sanasa Development)'),
(31, 'SLS (Sri Lanka Savings)'),
(32, 'SMIB (State Mortgage & Investment)');

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE `batch` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `mfd` date DEFAULT NULL,
  `exp` date DEFAULT NULL,
  `cost` decimal(12,2) DEFAULT NULL,
  `retail_price` decimal(12,2) DEFAULT NULL,
  `wholesale_price` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Demo'),
(2, 'Demo');

-- --------------------------------------------------------

--
-- Table structure for table `cheque`
--

CREATE TABLE `cheque` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `cheque_no` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `cheque_status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cheque_status`
--

CREATE TABLE `cheque_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cheque_status`
--

INSERT INTO `cheque_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Done'),
(3, 'Canceled'),
(4, 'Returned');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `route_id` int(1) NOT NULL DEFAULT 1,
  `address` varchar(500) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `balance` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `code`, `route_id`, `address`, `phone`, `email`, `balance`) VALUES
(13, 'aa', '11', 1, 'aa', '0714698713', 'admin@admin.com', '55.00');

-- --------------------------------------------------------

--
-- Table structure for table `customer_order`
--

CREATE TABLE `customer_order` (
  `id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customer_order_status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_product`
--

CREATE TABLE `customer_order_product` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `customer_order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order_status`
--

CREATE TABLE `customer_order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_order_status`
--

INSERT INTO `customer_order_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `customer_payment`
--

CREATE TABLE `customer_payment` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `daily_expences`
--

CREATE TABLE `daily_expences` (
  `id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `exp_date` date NOT NULL,
  `feed_date` datetime NOT NULL DEFAULT current_timestamp(),
  `expence_cat` int(11) NOT NULL,
  `Note` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deliverer`
--

CREATE TABLE `deliverer` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `route_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deliverer_inventory`
--

CREATE TABLE `deliverer_inventory` (
  `id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `deliverer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deliverer_user`
--

CREATE TABLE `deliverer_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deliverer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`) VALUES
(2, 'Demo Department');

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `name`) VALUES
(1, 'Manager'),
(2, 'Rep'),
(3, 'Driver'),
(5, 'Test'),
(6, 'STORE KEEPER'),
(7, 'MATHALE C'),
(8, 'Wijayananayakkara');

-- --------------------------------------------------------

--
-- Table structure for table `expence_cat`
--

CREATE TABLE `expence_cat` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grn`
--

CREATE TABLE `grn` (
  `id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `grn_type_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grn_material`
--

CREATE TABLE `grn_material` (
  `id` int(11) NOT NULL,
  `grn_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `volume` decimal(10,3) DEFAULT NULL,
  `unit_price` decimal(12,2) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grn_product`
--

CREATE TABLE `grn_product` (
  `id` int(11) NOT NULL,
  `grn_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grn_type`
--

CREATE TABLE `grn_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grn_type`
--

INSERT INTO `grn_type` (`id`, `name`) VALUES
(1, 'Product'),
(2, 'Material');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `reurn_invoice_id` int(11) DEFAULT NULL,
  `invoice_status_id` int(11) NOT NULL,
  `gross_amount` decimal(12,2) DEFAULT NULL,
  `net_amount` decimal(12,2) DEFAULT NULL,
  `balance` decimal(12,2) DEFAULT NULL,
  `customer_order_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `invoice_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `invoice_condition_id` int(11) NOT NULL,
  `deliverer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_condition`
--

CREATE TABLE `invoice_condition` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_condition`
--

INSERT INTO `invoice_condition` (`id`, `name`) VALUES
(1, 'New'),
(2, 'Return');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_inventory`
--

CREATE TABLE `invoice_inventory` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` decimal(12,2) DEFAULT NULL,
  `unit_discount` decimal(12,2) DEFAULT NULL,
  `gross_amount` decimal(12,2) DEFAULT NULL,
  `net_amount` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_return`
--

CREATE TABLE `invoice_return` (
  `id` int(11) NOT NULL,
  `date_time` datetime DEFAULT NULL,
  `invoice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `deliverer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_return_inventory`
--

CREATE TABLE `invoice_return_inventory` (
  `id` int(11) NOT NULL,
  `invoice_return_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `return_reason_id` int(11) NOT NULL,
  `unit_price` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_status`
--

CREATE TABLE `invoice_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_status`
--

INSERT INTO `invoice_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_type`
--

CREATE TABLE `invoice_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_type`
--

INSERT INTO `invoice_type` (`id`, `name`) VALUES
(1, 'Normal'),
(2, 'Retail');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `material_stock`
--

CREATE TABLE `material_stock` (
  `id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `volume` decimal(10,3) DEFAULT NULL,
  `grn_material_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `name`) VALUES
(1, 'User'),
(2, 'Privilege'),
(3, 'Designation'),
(4, 'Target'),
(5, 'Category'),
(6, 'Product'),
(7, 'Batch'),
(8, 'Material'),
(9, 'ProductionPlan'),
(10, 'Production'),
(11, 'Supplier'),
(12, 'Customer'),
(13, 'Route'),
(14, 'ProductPO'),
(15, 'MaterialPO'),
(16, 'ProductGRN'),
(17, 'MaterialGRN'),
(18, 'Invoice'),
(19, 'Payment'),
(20, 'Return'),
(21, 'Deliverer'),
(22, 'DelivererInventory'),
(23, 'MaterialStock'),
(24, 'Inventory'),
(25, 'Cheque'),
(26, 'Role'),
(27, 'Department');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `payment_method_id` int(11) NOT NULL,
  `payment_status_id` int(11) NOT NULL,
  `date_time` datetime DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `payment_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_cheque`
--

CREATE TABLE `payment_cheque` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `cheque_id` int(11) NOT NULL,
  `amount` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_invoice`
--

CREATE TABLE `payment_invoice` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `amount` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `name`) VALUES
(1, 'Cash'),
(2, 'Cheque'),
(3, 'Credit Note');

-- --------------------------------------------------------

--
-- Table structure for table `payment_status`
--

CREATE TABLE `payment_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_status`
--

INSERT INTO `payment_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Done'),
(3, 'Canceled');

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`) VALUES
(1, 'Invoice Payment'),
(2, 'Customer Payment');

-- --------------------------------------------------------

--
-- Table structure for table `petty_cash`
--

CREATE TABLE `petty_cash` (
  `id` int(11) NOT NULL,
  `reson` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `petty_date` datetime NOT NULL,
  `amount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `privilege`
--

CREATE TABLE `privilege` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `view` tinyint(1) DEFAULT NULL,
  `ins` tinyint(1) DEFAULT NULL,
  `upd` tinyint(1) DEFAULT NULL,
  `del` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privilege`
--

INSERT INTO `privilege` (`id`, `role_id`, `module_id`, `view`, `ins`, `upd`, `del`) VALUES
(1, 1, 2, 1, 1, 1, 1),
(2, 1, 1, 1, 1, 1, 1),
(3, 1, 3, 1, 1, 1, 1),
(4, 1, 4, 1, 1, 1, 1),
(5, 1, 5, 1, 1, 1, 1),
(6, 1, 6, 1, 1, 1, 1),
(7, 1, 7, 1, 1, 1, 1),
(8, 1, 8, 1, 1, 1, 1),
(9, 1, 9, 1, 1, 1, 1),
(10, 1, 10, 1, 1, 1, 1),
(11, 1, 11, 1, 1, 1, 1),
(12, 1, 12, 1, 1, 1, 1),
(13, 1, 13, 1, 1, 1, 1),
(14, 1, 14, 1, 1, 1, 1),
(15, 1, 15, 1, 1, 1, 1),
(16, 1, 16, 1, 1, 1, 1),
(17, 1, 17, 1, 1, 1, 1),
(18, 1, 18, 1, 1, 1, 1),
(19, 1, 19, 1, 1, 1, 1),
(20, 1, 20, 1, 1, 1, 1),
(21, 1, 21, 1, 1, 1, 1),
(22, 1, 22, 1, 1, 1, 1),
(23, 1, 23, 1, 1, 1, 1),
(24, 1, 24, 1, 1, 1, 1),
(25, 1, 25, 1, 1, 1, 1),
(26, 1, 26, 1, 1, 1, 1),
(27, 4, 1, 1, 1, 1, 0),
(28, 4, 2, 1, 1, 1, 0),
(29, 4, 3, 1, 1, 1, 0),
(30, 4, 4, 1, 1, 1, 0),
(31, 4, 5, 1, 1, 1, 0),
(32, 4, 6, 1, 1, 1, 0),
(33, 4, 7, 1, 1, 1, 0),
(34, 4, 8, 1, 1, 1, 0),
(35, 4, 9, 1, 1, 1, 0),
(36, 4, 10, 1, 1, 1, 0),
(37, 4, 11, 1, 1, 1, 0),
(38, 4, 12, 1, 1, 1, 0),
(39, 4, 13, 1, 1, 1, 0),
(40, 4, 14, 1, 1, 1, 0),
(41, 4, 15, 1, 1, 1, 0),
(42, 4, 16, 1, 1, 1, 1),
(43, 4, 17, 1, 1, 1, 0),
(44, 4, 18, 1, 1, 1, 0),
(45, 4, 19, 1, 1, 1, 0),
(46, 4, 20, 1, 1, 1, 0),
(47, 4, 21, 1, 1, 1, 0),
(48, 4, 22, 1, 1, 1, 0),
(49, 4, 23, 1, 1, 1, 0),
(50, 4, 24, 1, 1, 1, 0),
(51, 4, 25, 1, 1, 1, 0),
(52, 4, 26, 1, 1, 1, 0),
(53, 3, 1, 0, 0, 0, 0),
(54, 3, 2, 0, 0, 0, 0),
(55, 3, 3, 0, 0, 0, 0),
(56, 3, 4, 0, 0, 0, 0),
(57, 3, 5, 0, 0, 0, 0),
(58, 3, 6, 0, 0, 0, 0),
(59, 3, 7, 0, 0, 0, 0),
(60, 3, 8, 0, 0, 0, 0),
(61, 3, 9, 0, 0, 0, 0),
(62, 3, 10, 0, 0, 0, 0),
(63, 3, 11, 0, 0, 0, 0),
(64, 3, 12, 0, 0, 0, 0),
(65, 3, 13, 0, 0, 0, 0),
(66, 3, 14, 0, 0, 0, 0),
(67, 3, 15, 0, 0, 0, 0),
(68, 3, 16, 1, 1, 1, 1),
(69, 3, 17, 0, 0, 0, 0),
(70, 3, 18, 0, 0, 0, 0),
(71, 3, 19, 0, 0, 0, 0),
(72, 3, 20, 0, 0, 0, 0),
(73, 3, 21, 1, 1, 1, 1),
(74, 3, 22, 1, 1, 1, 1),
(75, 3, 23, 0, 0, 0, 0),
(76, 3, 24, 1, 1, 1, 1),
(77, 3, 25, 0, 0, 0, 0),
(78, 3, 26, 0, 0, 0, 0),
(79, 1, 27, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `roq` int(11) DEFAULT NULL,
  `max_qty` int(11) DEFAULT NULL,
  `min_qty` int(11) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `description` text NOT NULL,
  `exp_date_time` datetime NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `contract_period` varchar(50) NOT NULL,
  `department_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `shelf_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `roq`, `max_qty`, `min_qty`, `code`, `barcode`, `lot_number`, `description`, `exp_date_time`, `manufacturer`, `contract_period`, `department_id`, `location`, `shelf_number`) VALUES
(2, 'demoo', 1, 1, 1, 1, '1234', '15', 0, 'tharuka jhsaj dhkj', '2021-03-04 12:20:30', 'demo', '12 months', 0, 'demo', 1),
(3, 'demoo', 1, 1, 1, 1, '1234', '15', 1, '', '2021-03-04 11:36:20', 'demo', '12 months', 2, 'demo', 1),
(4, 'demoo', 1, 1, 1, 1, '1234', '15', 1, '', '2021-03-04 11:37:15', 'demo', '12 months', 2, 'demo', 1),
(5, 'demoo', 1, 1, 1, 1, '1234', '15', 1, '', '2021-03-04 11:37:15', 'demo', '12 months', 2, 'demo', 1),
(6, 'demoo', 1, 1, 1, 1, '1234', '15', 1, 'fdgfd fdgfd', '2021-03-04 11:38:47', 'demo', '12 months', 2, 'demo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `description` text DEFAULT NULL,
  `production_status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `production_material`
--

CREATE TABLE `production_material` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `volume` decimal(10,3) DEFAULT NULL,
  `wastage` decimal(10,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `production_material_stock`
--

CREATE TABLE `production_material_stock` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `material_stock_id` int(11) NOT NULL,
  `volume` decimal(10,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `production_product`
--

CREATE TABLE `production_product` (
  `id` int(11) NOT NULL,
  `production_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `production_status`
--

CREATE TABLE `production_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `production_status`
--

INSERT INTO `production_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Done'),
(3, 'Canceled');

-- --------------------------------------------------------

--
-- Table structure for table `product_return`
--

CREATE TABLE `product_return` (
  `id` int(11) NOT NULL,
  `date_time` varchar(45) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `deliverer_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `total_discount` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_return_batch`
--

CREATE TABLE `product_return_batch` (
  `id` int(11) NOT NULL,
  `product_return_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `unit_price` decimal(12,2) DEFAULT NULL,
  `discount` float DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_return_invoice`
--

CREATE TABLE `product_return_invoice` (
  `id` int(11) NOT NULL,
  `product_return_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `return_amount` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `purchase_order_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `purchase_order_status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_material`
--

CREATE TABLE `purchase_order_material` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `volume` decimal(10,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_product`
--

CREATE TABLE `purchase_order_product` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_status`
--

CREATE TABLE `purchase_order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_order_status`
--

INSERT INTO `purchase_order_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_type`
--

CREATE TABLE `purchase_order_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchase_order_type`
--

INSERT INTO `purchase_order_type` (`id`, `name`) VALUES
(1, 'Product'),
(2, 'Material');

-- --------------------------------------------------------

--
-- Table structure for table `return_reason`
--

CREATE TABLE `return_reason` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_reason`
--

INSERT INTO `return_reason` (`id`, `name`) VALUES
(1, 'Customer Return'),
(2, 'Re Stock'),
(3, 'Damage'),
(4, 'Expire');

-- --------------------------------------------------------

--
-- Table structure for table `return_table`
--

CREATE TABLE `return_table` (
  `id` int(11) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT current_timestamp(),
  `note` varchar(500) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deliverer_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'SUPER ADMIN'),
(2, 'ADMIN'),
(3, 'STORE ADMIN'),
(4, 'DATA ENTRY'),
(5, 'BOOK KEEPING'),
(6, 'Sales Executive'),
(7, 'MATHALE C');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`id`, `name`) VALUES
(1, 'default');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `department_id` int(11) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `fax` varchar(100) NOT NULL,
  `email` varchar(300) DEFAULT NULL,
  `contact_no` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `target`
--

CREATE TABLE `target` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_month_id` int(11) NOT NULL,
  `amount` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `target_month`
--

CREATE TABLE `target_month` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `user_status_id` int(11) NOT NULL,
  `name` varchar(400) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contact_no` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `nic` varchar(400) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `image` varchar(400) DEFAULT NULL,
  `soft_delete` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `designation_id`, `user_status_id`, `name`, `username`, `password`, `dob`, `contact_no`, `email`, `nic`, `address`, `image`, `soft_delete`) VALUES
(1, 1, 1, 'Admin', 'admin', '$2y$10$ZDUyNDYzYjYzM2VjZTNmMOkH2c0VKNI5.N43XGEQCXPHwSqInqNTW', '1995-05-05', '0336665552', 'aaa@bbb.com', '65465465v', '313/1 skjhsdkjcnbdkjcbnkdjnc', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role_id`, `user_id`) VALUES
(2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_status`
--

CREATE TABLE `user_status` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_status`
--

INSERT INTO `user_status` (`id`, `name`) VALUES
(1, 'Active'),
(2, 'Deactive');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_activity_user1_idx` (`user_id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch`
--
ALTER TABLE `batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_batch_product1_idx` (`product_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cheque`
--
ALTER TABLE `cheque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cheque_cheque_status1_idx` (`cheque_status_id`),
  ADD KEY `fk_cheque_bank1_idx` (`bank_id`);

--
-- Indexes for table `cheque_status`
--
ALTER TABLE `cheque_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_order`
--
ALTER TABLE `customer_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order_customer1_idx` (`customer_id`),
  ADD KEY `fk_order_user1_idx` (`user_id`),
  ADD KEY `fk_customer_order_customer_order_status1_idx` (`customer_order_status_id`);

--
-- Indexes for table `customer_order_product`
--
ALTER TABLE `customer_order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order_has_product_product1_idx` (`product_id`),
  ADD KEY `fk_order_product_customer_order1_idx` (`customer_order_id`);

--
-- Indexes for table `customer_order_status`
--
ALTER TABLE `customer_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_payment`
--
ALTER TABLE `customer_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_customer_has_payment_payment1_idx` (`payment_id`),
  ADD KEY `fk_customer_has_payment_customer1_idx` (`customer_id`);

--
-- Indexes for table `daily_expences`
--
ALTER TABLE `daily_expences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expence_cat` (`expence_cat`);

--
-- Indexes for table `deliverer`
--
ALTER TABLE `deliverer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_distributer_route1_idx` (`route_id`);

--
-- Indexes for table `deliverer_inventory`
--
ALTER TABLE `deliverer_inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_distributer_has_inventory_inventory1_idx` (`inventory_id`),
  ADD KEY `fk_deliverer_inventory_deliverer1_idx` (`deliverer_id`);

--
-- Indexes for table `deliverer_user`
--
ALTER TABLE `deliverer_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_distributer_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_deliverer_user_deliverer1_idx` (`deliverer_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expence_cat`
--
ALTER TABLE `expence_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grn`
--
ALTER TABLE `grn`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grn_purchase_order1_idx` (`purchase_order_id`),
  ADD KEY `fk_grn_user1_idx` (`user_id`),
  ADD KEY `fk_grn_grn_type1_idx` (`grn_type_id`),
  ADD KEY `fk_grn_supplier1_idx` (`supplier_id`);

--
-- Indexes for table `grn_material`
--
ALTER TABLE `grn_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grn_has_material_material1_idx` (`material_id`),
  ADD KEY `fk_grn_has_material_grn1_idx` (`grn_id`),
  ADD KEY `fk_grn_material_user1_idx` (`user_id`);

--
-- Indexes for table `grn_product`
--
ALTER TABLE `grn_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grn_has_product_grn1_idx` (`grn_id`),
  ADD KEY `fk_grn_product_batch1_idx` (`batch_id`),
  ADD KEY `fk_grn_product_user1_idx` (`user_id`);

--
-- Indexes for table `grn_type`
--
ALTER TABLE `grn_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_inventory_product1_idx` (`product_id`),
  ADD KEY `fk_inventory_batch1_idx` (`batch_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_invoice_customer1_idx` (`customer_id`),
  ADD KEY `fk_invoice_invoice1_idx` (`reurn_invoice_id`),
  ADD KEY `fk_invoice_invoice_status1_idx` (`invoice_status_id`),
  ADD KEY `fk_invoice_customer_order1_idx` (`customer_order_id`),
  ADD KEY `fk_invoice_invoice_type1_idx` (`invoice_type_id`),
  ADD KEY `fk_invoice_user1_idx` (`user_id`),
  ADD KEY `fk_invoice_invoice_condition1_idx` (`invoice_condition_id`),
  ADD KEY `fk_invoice_deliverer1_idx` (`deliverer_id`);

--
-- Indexes for table `invoice_condition`
--
ALTER TABLE `invoice_condition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_inventory`
--
ALTER TABLE `invoice_inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_invoice_product_invoice1_idx` (`invoice_id`),
  ADD KEY `fk_invoice_product_inventory1_idx` (`inventory_id`);

--
-- Indexes for table `invoice_return`
--
ALTER TABLE `invoice_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_return_invoice1_idx` (`invoice_id`),
  ADD KEY `fk_return_user1_idx` (`user_id`),
  ADD KEY `fk_invoice_return_deliverer1_idx` (`deliverer_id`);

--
-- Indexes for table `invoice_return_inventory`
--
ALTER TABLE `invoice_return_inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_return_has_inventory_inventory1_idx` (`inventory_id`),
  ADD KEY `fk_product_return_inventory_invoice_return1_idx` (`invoice_return_id`),
  ADD KEY `fk_invoice_return_inventory_return_reason1_idx` (`return_reason_id`);

--
-- Indexes for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_type`
--
ALTER TABLE `invoice_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_stock`
--
ALTER TABLE `material_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_material_stock_material1_idx` (`material_id`),
  ADD KEY `fk_material_stock_grn_material1_idx` (`grn_material_id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_invoice_payment_payment_method1_idx` (`payment_method_id`),
  ADD KEY `fk_invoice_payment_user1_idx` (`user_id`),
  ADD KEY `fk_payment_payment_status1_idx` (`payment_status_id`),
  ADD KEY `fk_payment_payment_type1_idx` (`payment_type_id`);

--
-- Indexes for table `payment_cheque`
--
ALTER TABLE `payment_cheque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_invoice_payment_has_cheque_cheque1_idx` (`cheque_id`),
  ADD KEY `fk_invoice_payment_has_cheque_invoice_payment1_idx` (`payment_id`);

--
-- Indexes for table `payment_invoice`
--
ALTER TABLE `payment_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_payment_invoice_payment1_idx` (`payment_id`),
  ADD KEY `fk_payment_invoice_invoice1_idx` (`invoice_id`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_status`
--
ALTER TABLE `payment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petty_cash`
--
ALTER TABLE `petty_cash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_has_module_module1_idx` (`module_id`),
  ADD KEY `fk_role_has_module_role1_idx` (`role_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_category_idx` (`category_id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_production_production_status1_idx` (`production_status_id`);

--
-- Indexes for table `production_material`
--
ALTER TABLE `production_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_recipie_material_material1_idx` (`material_id`),
  ADD KEY `fk_recipie_material_production1_idx` (`production_id`);

--
-- Indexes for table `production_material_stock`
--
ALTER TABLE `production_material_stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_production_has_material_stock_material_stock1_idx` (`material_stock_id`),
  ADD KEY `fk_production_has_material_stock_production1_idx` (`production_id`);

--
-- Indexes for table `production_product`
--
ALTER TABLE `production_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_production_has_product_production1_idx` (`production_id`),
  ADD KEY `fk_production_product_batch1_idx` (`batch_id`);

--
-- Indexes for table `production_status`
--
ALTER TABLE `production_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_return`
--
ALTER TABLE `product_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_return_user1_idx` (`user_id`),
  ADD KEY `fk_product_return_deliverer1_idx` (`deliverer_id`),
  ADD KEY `fk_product_return_customer1_idx` (`customer_id`),
  ADD KEY `fk_product_return_invoice1_idx` (`invoice_id`);

--
-- Indexes for table `product_return_batch`
--
ALTER TABLE `product_return_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_return_has_batch_batch1_idx` (`batch_id`),
  ADD KEY `fk_product_return_has_batch_product_return1_idx` (`product_return_id`),
  ADD KEY `fk_product_return_batch_return_reason1_idx` (`return_reason_id`);

--
-- Indexes for table `product_return_invoice`
--
ALTER TABLE `product_return_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_return_has_invoice_invoice1_idx` (`invoice_id`),
  ADD KEY `fk_product_return_has_invoice_product_return1_idx` (`product_return_id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_purchase_order_supplier1_idx` (`supplier_id`),
  ADD KEY `fk_purchase_order_purchase_order_type1_idx` (`purchase_order_type_id`),
  ADD KEY `fk_purchase_order_user1_idx` (`user_id`),
  ADD KEY `fk_purchase_order_purchase_order_status1_idx` (`purchase_order_status_id`);

--
-- Indexes for table `purchase_order_material`
--
ALTER TABLE `purchase_order_material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_purchase_order_has_material_material1_idx` (`material_id`),
  ADD KEY `fk_purchase_order_has_material_purchase_order1_idx` (`purchase_order_id`);

--
-- Indexes for table `purchase_order_product`
--
ALTER TABLE `purchase_order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_purchase_order_has_product_product1_idx` (`product_id`),
  ADD KEY `fk_purchase_order_has_product_purchase_order1_idx` (`purchase_order_id`);

--
-- Indexes for table `purchase_order_status`
--
ALTER TABLE `purchase_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_type`
--
ALTER TABLE `purchase_order_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_reason`
--
ALTER TABLE `return_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_table`
--
ALTER TABLE `return_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `deliverer_id` (`deliverer_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `target`
--
ALTER TABLE `target`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_target_user1_idx` (`user_id`),
  ADD KEY `fk_target_target_month1_idx` (`target_month_id`);

--
-- Indexes for table `target_month`
--
ALTER TABLE `target_month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_designation1_idx` (`designation_id`),
  ADD KEY `fk_user_user_status1_idx` (`user_status_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_has_role_role1_idx` (`role_id`),
  ADD KEY `fk_user_role_user1_idx` (`user_id`);

--
-- Indexes for table `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `batch`
--
ALTER TABLE `batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cheque`
--
ALTER TABLE `cheque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cheque_status`
--
ALTER TABLE `cheque_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `customer_order`
--
ALTER TABLE `customer_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_order_product`
--
ALTER TABLE `customer_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer_order_status`
--
ALTER TABLE `customer_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_payment`
--
ALTER TABLE `customer_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_expences`
--
ALTER TABLE `daily_expences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliverer`
--
ALTER TABLE `deliverer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliverer_inventory`
--
ALTER TABLE `deliverer_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliverer_user`
--
ALTER TABLE `deliverer_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `expence_cat`
--
ALTER TABLE `expence_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grn`
--
ALTER TABLE `grn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grn_material`
--
ALTER TABLE `grn_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grn_product`
--
ALTER TABLE `grn_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grn_type`
--
ALTER TABLE `grn_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_condition`
--
ALTER TABLE `invoice_condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice_inventory`
--
ALTER TABLE `invoice_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_return`
--
ALTER TABLE `invoice_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_return_inventory`
--
ALTER TABLE `invoice_return_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_status`
--
ALTER TABLE `invoice_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice_type`
--
ALTER TABLE `invoice_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `material_stock`
--
ALTER TABLE `material_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_cheque`
--
ALTER TABLE `payment_cheque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_invoice`
--
ALTER TABLE `payment_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_status`
--
ALTER TABLE `payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `petty_cash`
--
ALTER TABLE `petty_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `privilege`
--
ALTER TABLE `privilege`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `production_material`
--
ALTER TABLE `production_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `production_material_stock`
--
ALTER TABLE `production_material_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `production_product`
--
ALTER TABLE `production_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `production_status`
--
ALTER TABLE `production_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_return`
--
ALTER TABLE `product_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_return_batch`
--
ALTER TABLE `product_return_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_return_invoice`
--
ALTER TABLE `product_return_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_order_material`
--
ALTER TABLE `purchase_order_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_order_product`
--
ALTER TABLE `purchase_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_order_status`
--
ALTER TABLE `purchase_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchase_order_type`
--
ALTER TABLE `purchase_order_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `return_reason`
--
ALTER TABLE `return_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `return_table`
--
ALTER TABLE `return_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `route`
--
ALTER TABLE `route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `target`
--
ALTER TABLE `target`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `target_month`
--
ALTER TABLE `target_month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `fk_activity_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `batch`
--
ALTER TABLE `batch`
  ADD CONSTRAINT `fk_batch_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cheque`
--
ALTER TABLE `cheque`
  ADD CONSTRAINT `fk_cheque_bank1` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cheque_cheque_status1` FOREIGN KEY (`cheque_status_id`) REFERENCES `cheque_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_order`
--
ALTER TABLE `customer_order`
  ADD CONSTRAINT `fk_customer_order_customer_order_status1` FOREIGN KEY (`customer_order_status_id`) REFERENCES `customer_order_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_order_product`
--
ALTER TABLE `customer_order_product`
  ADD CONSTRAINT `fk_order_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_product_customer_order1` FOREIGN KEY (`customer_order_id`) REFERENCES `customer_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_payment`
--
ALTER TABLE `customer_payment`
  ADD CONSTRAINT `fk_customer_has_payment_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_customer_has_payment_payment1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `daily_expences`
--
ALTER TABLE `daily_expences`
  ADD CONSTRAINT `daily_expences_ibfk_1` FOREIGN KEY (`expence_cat`) REFERENCES `expence_cat` (`id`);

--
-- Constraints for table `deliverer`
--
ALTER TABLE `deliverer`
  ADD CONSTRAINT `fk_distributer_route1` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `deliverer_inventory`
--
ALTER TABLE `deliverer_inventory`
  ADD CONSTRAINT `fk_deliverer_inventory_deliverer1` FOREIGN KEY (`deliverer_id`) REFERENCES `deliverer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_distributer_has_inventory_inventory1` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `deliverer_user`
--
ALTER TABLE `deliverer_user`
  ADD CONSTRAINT `fk_deliverer_user_deliverer1` FOREIGN KEY (`deliverer_id`) REFERENCES `deliverer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_distributer_has_user_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `grn`
--
ALTER TABLE `grn`
  ADD CONSTRAINT `fk_grn_grn_type1` FOREIGN KEY (`grn_type_id`) REFERENCES `grn_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grn_purchase_order1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grn_supplier1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grn_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `grn_material`
--
ALTER TABLE `grn_material`
  ADD CONSTRAINT `fk_grn_has_material_grn1` FOREIGN KEY (`grn_id`) REFERENCES `grn` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grn_has_material_material1` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grn_material_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `grn_product`
--
ALTER TABLE `grn_product`
  ADD CONSTRAINT `fk_grn_has_product_grn1` FOREIGN KEY (`grn_id`) REFERENCES `grn` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grn_product_batch1` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grn_product_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `fk_inventory_batch1` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inventory_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_invoice_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_customer_order1` FOREIGN KEY (`customer_order_id`) REFERENCES `customer_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_deliverer1` FOREIGN KEY (`deliverer_id`) REFERENCES `deliverer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_invoice1` FOREIGN KEY (`reurn_invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_invoice_condition1` FOREIGN KEY (`invoice_condition_id`) REFERENCES `invoice_condition` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_invoice_status1` FOREIGN KEY (`invoice_status_id`) REFERENCES `invoice_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_invoice_type1` FOREIGN KEY (`invoice_type_id`) REFERENCES `invoice_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_inventory`
--
ALTER TABLE `invoice_inventory`
  ADD CONSTRAINT `fk_invoice_product_inventory1` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_return`
--
ALTER TABLE `invoice_return`
  ADD CONSTRAINT `fk_invoice_return_deliverer1` FOREIGN KEY (`deliverer_id`) REFERENCES `deliverer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_return_inventory`
--
ALTER TABLE `invoice_return_inventory`
  ADD CONSTRAINT `fk_invoice_return_inventory_return_reason1` FOREIGN KEY (`return_reason_id`) REFERENCES `return_reason` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_return_inventory_invoice_return1` FOREIGN KEY (`invoice_return_id`) REFERENCES `invoice_return` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_has_inventory_inventory1` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `material_stock`
--
ALTER TABLE `material_stock`
  ADD CONSTRAINT `fk_material_stock_grn_material1` FOREIGN KEY (`grn_material_id`) REFERENCES `grn_material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_material_stock_material1` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_invoice_payment_payment_method1` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_payment_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_payment_status1` FOREIGN KEY (`payment_status_id`) REFERENCES `payment_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_payment_type1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment_cheque`
--
ALTER TABLE `payment_cheque`
  ADD CONSTRAINT `fk_invoice_payment_has_cheque_cheque1` FOREIGN KEY (`cheque_id`) REFERENCES `cheque` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_payment_has_cheque_invoice_payment1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment_invoice`
--
ALTER TABLE `payment_invoice`
  ADD CONSTRAINT `fk_payment_invoice_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_invoice_payment1` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `privilege`
--
ALTER TABLE `privilege`
  ADD CONSTRAINT `fk_role_has_module_module1` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_role_has_module_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `production`
--
ALTER TABLE `production`
  ADD CONSTRAINT `fk_production_production_status1` FOREIGN KEY (`production_status_id`) REFERENCES `production_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `production_material`
--
ALTER TABLE `production_material`
  ADD CONSTRAINT `fk_recipie_material_material1` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recipie_material_production1` FOREIGN KEY (`production_id`) REFERENCES `production` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `production_material_stock`
--
ALTER TABLE `production_material_stock`
  ADD CONSTRAINT `fk_production_has_material_stock_material_stock1` FOREIGN KEY (`material_stock_id`) REFERENCES `material_stock` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_production_has_material_stock_production1` FOREIGN KEY (`production_id`) REFERENCES `production` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `production_product`
--
ALTER TABLE `production_product`
  ADD CONSTRAINT `fk_production_has_product_production1` FOREIGN KEY (`production_id`) REFERENCES `production` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_production_product_batch1` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_return`
--
ALTER TABLE `product_return`
  ADD CONSTRAINT `fk_product_return_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_return_deliverer1` FOREIGN KEY (`deliverer_id`) REFERENCES `deliverer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_return_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_return_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_return_batch`
--
ALTER TABLE `product_return_batch`
  ADD CONSTRAINT `fk_product_return_batch_return_reason1` FOREIGN KEY (`return_reason_id`) REFERENCES `return_reason` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_return_has_batch_batch1` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_return_has_batch_product_return1` FOREIGN KEY (`product_return_id`) REFERENCES `product_return` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_return_invoice`
--
ALTER TABLE `product_return_invoice`
  ADD CONSTRAINT `fk_product_return_has_invoice_invoice1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_return_has_invoice_product_return1` FOREIGN KEY (`product_return_id`) REFERENCES `product_return` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD CONSTRAINT `fk_purchase_order_purchase_order_status1` FOREIGN KEY (`purchase_order_status_id`) REFERENCES `purchase_order_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_order_purchase_order_type1` FOREIGN KEY (`purchase_order_type_id`) REFERENCES `purchase_order_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_order_supplier1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_order_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchase_order_material`
--
ALTER TABLE `purchase_order_material`
  ADD CONSTRAINT `fk_purchase_order_has_material_material1` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_order_has_material_purchase_order1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `purchase_order_product`
--
ALTER TABLE `purchase_order_product`
  ADD CONSTRAINT `fk_purchase_order_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_order_has_product_purchase_order1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `target`
--
ALTER TABLE `target`
  ADD CONSTRAINT `fk_target_target_month1` FOREIGN KEY (`target_month_id`) REFERENCES `target_month` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_target_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_designation1` FOREIGN KEY (`designation_id`) REFERENCES `designation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_user_status1` FOREIGN KEY (`user_status_id`) REFERENCES `user_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_user_has_role_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_role_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
