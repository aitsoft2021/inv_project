<?php
require_once './../util/initialize.php';
include 'common/upper_content.php';
?>

<!--page content-->

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">

      <div class="title_left">
        <h3>Item Wise Details</h3>
      </div>

      <div class="title_right">
        <button type="button" id="btn_print" class="btn btn-default" style="float: right;"><i class="glyphicon glyphicon-print"></i>  Print</button>
      </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">

      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title" style="background-color: #424242;color:white;border-radius: 5px 5px 0px 0px;"><h3><center> Search Details </center></h3></div>
            <div class="x_content">

              <form class="form-horizontal" method="post" action="item_wise_detailed_report_new_testing.php" >

                <div class="form-group">
                  <label class="control-label col-sm-2">Product:</label>
                  <div class="col-sm-10">
                    <select class="form-control selectpicker" data-show-subtext="true" data-live-search="true" name="item_name" required>
                      <?php
                      foreach (Product::find_all() as $data) {
                        echo "<option value='".$data->id."'>".$data->name." || ".$data->description."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>


                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">View</button>
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>

      <div class="row" id="print_div">

        <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
          <div class="x_panel">

            <div class="x_content">

              <p><b>ITEM NAME: <?php if(isset($_POST['item_name'])){ $data=Product::find_by_id($_POST['item_name']); echo $data->name; } ?></b></p>
              <p style="font-size:11px;"> PRINTERD DATE: <?php echo date("Y:m:d"); ?> || PRINT BY: <?php echo  $_SESSION["user"]["name"]; ?></p>

              </div>

            </div>
          </div>


          <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="x_panel">

              <div class="x_content">
                <div class="table-responsive">

                  <table id="sata_table" class="table table-bordered " cellspacing="0" width="100%">
                    <thead>
                      <tr style='font-size:10px;'>

                        <th>Type</th>
                        <th style='text-align: center;'>Code</th>
                        <th style='text-align: center;'>Date</th>
                        <th style='text-align: center;'>Customer</th>
                        <th style='text-align: center;'>Qty</th>
                        <th style="text-align:right;">Cost</th>
                        <th style="text-align:right;">Retail</th>

                      </tr>
                    </thead>
                    <tbody id="table_body">

                      <?php
                      $inventory_qty = 0;
                      if( isset($_POST['item_name']) ){
                        $item_name = $_POST['item_name'];
                        $item_names = Product::find_by_id($item_name)->name;
                        $today_date = date('Y-m-d');
                        $date = $today_date;
                        $date1 = str_replace('-', '/', $date);
                        $today_date = date('Y-m-d',strtotime($date1 . "+1 days"));

                        $period = new DatePeriod(
                          new DateTime('2020-01-01'),
                          new DateInterval('P1D'),
                          new DateTime($today_date)
                        );

                        // INITIAL STOCK RECORD
                        $balanceqty = 0;
                        $oldqty = 0;


                        foreach ($period as $key => $value) {
                          // echo $value->format('Y-m-d');
                          $current = $value->format('Y-m-d');

                          // grn balance calculation
                          $invoice_grn_total = 0;
                          foreach (GRN::find_all_by_date($current) as $data) {
                            foreach (GRNProduct::find_all_by_grn_id($data->id) as $dataw) {
                              if( $dataw->batch_id()->product_id == $item_name ){
                                $balanceqty = $balanceqty + $dataw->qty;
                                echo "<tr style='font-size:10px;'>";
                                echo "<td>GRN</td>";
                                echo "<td style='text-align: center;'>".$dataw->grn_id()->code."</td>";
                                echo "<td style='text-align: center;'>".$dataw->grn_id()->date_time."</td>";
                                echo "<td></td>";                                        ;
                                echo "<td style='text-align: center;'>".$dataw->qty."</td>";
                                echo "<td style='text-align: right;'>".$dataw->batch_id()->cost."</td>";
                                echo "<td style='text-align: right;'>".$dataw->batch_id()->retail_price."</td>";
                                echo "</tr>";

                              }
                            }
                          }


                          // activity log

                          // foreach (Activity::find_all_by_date($current, $item_names) as $datass) {
                          //   echo "<tr style='font-size:10px;'>";
                          //
                          //
                          //     echo "<td>STOCK ADJESTMENT</td>";
                          //     echo "<td style='text-align: center;'>AD".$datass->id."</td>";
                          //
                          //   echo "<td style='text-align: center;'>".$datass->date_time."</td>";
                          //   echo "<td style='text-align: center;'> - </td>";
                          //   echo "<td style='text-align: center;'> </td>";
                          //   echo "<td style='text-align: center;'> </td>";
                          //
                          //   echo "<td style='text-align: right;'> </td>";
                          //   echo "<td style='text-align: right;'> </td>";
                          //   echo "</tr>";
                          // }

                          // product return calculation

                          foreach(ProductReturn::find_all_by_product_id_date($item_name,$current) as $return_data){
                            echo "<tr style='font-size:10px;'>";
                            echo "<td>RETURN</td>";
                            $dt = new DateTime($return_data->date_time);
                            echo "<td style='text-align: center;'>".$return_data->id."</td>";
                            echo "<td style='text-align: center;'>".$return_data->date_time."</td>";
                            $return_invoice_data = ProductReturnInvoice::find_by_product_return_id($return_data->id);
                            echo "<td style='text-align:center;'>".$return_invoice_data->invoice_id()->customer_id()->name."</td>";
                            $return_batch_data = ProductReturnBatch::find_all_by_product_return_id($return_data->id);
                            foreach($return_batch_data as $view_batch_data){
                              if($view_batch_data->batch_id()->product_id == $item_name){
                                $balanceqty = $balanceqty + $view_batch_data->qty;
                                echo "<td style='text-align:center;'>".$view_batch_data->qty."</td>";
                                echo "<td style='text-align:right;'>".$view_batch_data->batch_id()->cost."</td>";
                                echo "<td style='text-align:right;'>".$view_batch_data->batch_id()->retail_price."</td>";
                              }
                            }
                            echo "</tr>";
                          }

                          // invoice balance calculation
                          $invoice_qty_total = 0;
                          $qty_calculation = 0;
                          foreach (Invoice::find_all_by_date($current) as $data) {
                            foreach (InvoiceInventory::find_all_by_invoice_id($data->id) as $datas) {
                              if( $datas->inventory_id()->product_id == $item_name ){
                                // echo $balanceqty;
                                $balanceqty = $balanceqty - $datas->qty;
                                echo "<tr style='font-size:10px;'>";
                                echo "<td>INVOICE</td>";
                                echo "<td style='text-align: center;'>".$datas->invoice_id()->code."</td>";
                                echo "<td style='text-align: center;'>".$datas->invoice_id()->date_time."</td>";
                                echo "<td style='text-align: center;'>".$datas->invoice_id()->customer_id()->name."</td>";
                                echo "<td style='text-align: center;'>".$datas->qty."</td>";
                                $qty_calculation = $qty_calculation + $datas->qty;

                                  echo "<td style='text-align: right;'>".$datas->inventory_id()->batch_id()->cost."</td>";
                                  echo "<td style='text-align: right;'>".$datas->inventory_id()->batch_id()->retail_price."</td>";

                                echo "</tr>";
                              }
                            }
                          }
                        }

                      }
                      ?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->

  <?php include 'common/bottom_content.php'; ?>

  <script>




  $(function () {
    $("#dtpFrom").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd'
    });
  });

  $(function () {
    $("#dtpTo").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd'
    });
  });

  //print_div

  $('#btn_print').click(function () {
    PrintDiv();
  });

  function PrintDiv() {
    var divToPrint = document.getElementById('print_div');
    var popupWin = window.open('', '_blank', 'width=800,height=500');
    popupWin.document.open();
    popupWin.document.write('<html><head><link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"></head><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
    popupWin.document.close();
  }
</script>
