<?php
require_once './../util/initialize.php';
include 'common/upper_content.php';

?>

<!--page content--> 

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Product Management</h3>
            </div>

            <div class="title_right">

            </div>
        </div>

        <div class="clearfix"></div>
        
        <?php Functions::output_result(); ?>
        
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <a href="product.php" target="_blank"><button id="btnNew" type="button" class="btn btn-round btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="glyphicon glyphicon-plus"></i> Add New</button></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>Barcode</th>
                                    <th>Lot Number</th>
                                    <th>Name</th>
                                    <th>Item Type</th>
                                    <th>Description</th>
                                    <th>Exp Date</th>
                                    <th>Manufacturer</th>
                                    <th>Contract Period</th>
                                    <th>Department</th>
                                    <th>Location</th>
                                    <th>Shelf Number</th>
                                    <th>ROQ</th>
                                    <th>Max Qty</th>
                                    <th>Min Qty</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                $total_records = Product::row_count();
                                $pagination = new Pagination($total_records);
                                $objects = Product::find_all_by_limit_offset($pagination->records_per_page, $pagination->offset());
                                $department = new Department();
                                foreach ($objects as $product) {
                                    ?>
                                    <tr>
                                        <td><?php echo $product->id ?></td>
                                        <td><?php echo $product->code ?></td>
                                        <td><?php echo $product->barcode ?></td>
                                        <td><?php echo $product->lot_number ?></td>
                                        <td><?php echo $product->name ?></td>
                                        <td><?php echo $product->category_id()->name ?></td>
                                        <td><?php echo $product->description ?></td>
                                        <td><?php echo $product->exp_date_time ?></td>
                                        <td><?php echo $product->manufacturer ?></td>
                                        <td><?php echo $product->contract_period ?></td>
                                        <td><?php 
                                        if(empty($product->department_id)){
                                                     echo '';
                                        }else{
                                            $department=Department::find_by_id($product->department_id);
                                            echo $department->name;
                                        }  
                                        ?></td>
                                        <td><?php echo $product->location ?></td>
                                        <td><?php echo $product->shelf_number ?></td>
                                        <td><?php echo $product->roq ?></td>
                                        <td><?php echo $product->max_qty ?></td>
                                        <td><?php echo $product->min_qty ?></td>
                                        <td>
                                            <a href="product.php?id=<?php echo Functions::custom_crypt($product->id); ?>">
                                                <button class="btn btn-primary btn-xs" ><i class="glyphicon glyphicon-edit"></i> Edit</button>
                                            </a>
<!--                                            <form action="user_profile.php" method="post" target="_blank" >
                                                <input type="hidden" name="id" value="<?php // echo $product->id ?>"/>
                                                <button type="submit" name="view" class="btn btn-primary btn-xs" ><i class="glyphicon glyphicon-new-window"></i> View</button>
                                            </form>-->
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="x_panel">
                    <div onclick="window.location.href:''" class="x_content">
                        <?php
                        echo $pagination->get_pagination_links_html1("product_management.php");
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--/page content--> 
<?php include 'common/bottom_content.php'; ?>

<script>
    window.onfocus = function () {
//        location.reload(); 
    };
</script>