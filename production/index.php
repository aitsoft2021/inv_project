<?php
require_once './../util/initialize.php';

include 'common/upper_content.php';
?>
<!-- page content -->
<div class="right_col" role="main" style="background-color:white;">
  <div class="">
    <div class="row">

      <div class="row tile_count">

        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count" style="text-align:center;">
          <span class="count_top"><i class="fa fa-user"></i> PLACE A GRN</span>
          <div class="count"><a href="product_grn.php" style="padding:20px;" class="btn btn-primary btn-block" role="button">ADD PPRODUCT GRN</a></div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count" style="text-align:center;">
          <span class="count_top"><i class="fa fa-user"></i> Place A Payment</span>
          <div class="count"><a href="invoice_by_deliverer.php" style="padding:20px;" class="btn btn-primary btn-block" role="button">ADD INVOICE</a></div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count" style="text-align:center;">
          <span class="count_top"><i class="fa fa-user"></i> Make A Payment</span>
          <div class="count green"><a href="payment.php" style="padding:20px;" class="btn btn-primary btn-block" role="button">ADD PAYMENT</a></div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count" style="text-align:center;">
          <span class="count_top"><i class="fa fa-user"></i> Place A Return</span>
          <div class="count"><a href="product_return_by_deliverer_new.php" style="padding:20px;" class="btn btn-primary btn-block" role="button">PRODUCT RETURN</a></div>
        </div>



      </div>

      <div class="page-title">


        <div class="title_right">

        </div>
      </div>

      <!-- data slider start -->

      <div id="myCarousel" class="carousel slide" data-ride="carousel">


        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">

            

          </div>

          <div class="item">

            

          </div>


        </div>


      </div>

      <!-- data slider ends -->

      <div class="clearfix"></div>
      <?php Functions::output_result(); ?>
      <!-- <div class="row">

        

        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2 id="title">Daily Expences Quick Add</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <form action="proccess/daily_expence_process.php" method="post">

                <div class="form-group">
                  <label for="pwd">Expence Category:</label>
                  <select class="form-control" name="expence_cat" required>
                    <?php

                    foreach(ExpenceCat::find_all() as $expencecat){
                      echo "<option value='".$expencecat->id."'>".$expencecat->cat_name."</option>";
                    }

                    ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="pwd">Amount:</label>
                  <input type="text" class="form-control" name="amount" required>
                </div>

                <div class="form-group">
                  <label for="pwd">Expence Date:</label>
                  <input type="text" class="form-control" placeholder="Date" name="exp_date" value="<?php echo date("Y-m-d"); ?>" id="dtpChequeDate" required>
                </div>

                <div class="form-group">
                  <label for="pwd">Special Note:</label>
                  <textarea class="form-control" name="Note"></textarea>
                </div>

                <button type="submit" name="save" class="btn btn-primary btn-block">SAVE</button>
              </form>

            </div>
          </div>
        </div>
      </div> -->

    </div>
  </div>
</div>
<!-- /page content -->
<?php include 'common/bottom_content.php'; ?>

