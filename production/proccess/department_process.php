<?php

require_once './../../util/initialize.php';

if (isset($_POST['save'])) {
    if (Functions::check_privilege_redirect("Department", "ins", "./../index.php")) {
        $department = new Department();
        $department->name = trim($_POST['name']);

        try {
            $department->save();
            Activity::log_action("Department saved - " . $department->name);
            $_SESSION["message"] = "Successfully saved.";
            Functions::redirect_to("./../department.php");
        } catch (Exception $exc) {
            $_SESSION["error"] = "Error..! Failed to save.";
            Functions::redirect_to("./../department.php");
        }
    }
}

if (isset($_POST['update'])) {
    if (Functions::check_privilege_redirect("Department", "upd", "./../index.php")) {
        $department = Department::find_by_id($_POST['id']);
        $department->name = trim($_POST['name']);

        try {
            $department->save();
            Activity::log_action("Department updated - " . $department->name);
            $_SESSION["message"] = "Successfully updated.";
            Functions::redirect_to("./../department.php");
        } catch (Exception $exc) {
            $_SESSION["error"] = "Error..! Failed to update.";
            Functions::redirect_to("./../department.php");
        }
    }
}


if (isset($_POST['delete'])) {
    if (Functions::check_privilege_redirect("Department", "del", "./../index.php")) {
        $department = Department::find_by_id($_POST["id"]);
        try {
            $department->delete();
            Activity::log_action("Department deleted - " . $department->name);
            $_SESSION["message"] = "Successfully deleted.";
            Functions::redirect_to("./../department.php");
        } catch (Exception $exc) {
            $products = Product::find_all_by_category_id($department->id);
            if (!empty($products)) {
                $_SESSION["error"] = "Could not delete.. Products already added to the selected Department";
                Functions::redirect_to("./../department.php");
            } else {
                $_SESSION["error"] = "Error..! Failed to deleted.";
                Functions::redirect_to("./../department.php");
            }
        }
    }
}
?>

