<?php

require_once './../../util/initialize.php';

if (isset($_POST['save'])) {
    $product = new Product();
    $product->name = trim($_POST['name']);//required
    $product->code = trim($_POST['code']);//required
    $product->category_id = trim($_POST['category_id']);//required
    $product->roq = trim($_POST['roq']);//required
    $product->max_qty = trim($_POST['max_qty']);//required
    $product->min_qty = trim($_POST['min_qty']);//required
    //new fields 
    $product->barcode = trim($_POST['barcode']);//required
    $product->lot_number = trim($_POST['lot_number']);
    $product->description = trim($_POST['description']);
    $product->exp_date_time = trim($_POST['exp_date_time']);
    $product->manufacturer = trim($_POST['manufacturer']);
    $product->contract_period = trim($_POST['contract_period']);
    $product->department_id = trim((isset($_POST['department_id'])) ? $_POST['department_id'] : '');
    $product->location = trim($_POST['location']);
    $product->shelf_number = trim($_POST['shelf_number']);
    

    try {
        $product->save();
        Activity::log_action("Product - saved : ".$product->name);
        $_SESSION["message"] = "Successfully saved.";
        Functions::redirect_to("./../product_management.php");
    } catch (Exception $exc) {
        $_SESSION["error"] = "Error..! Failed to save.";
        Functions::redirect_to("./../product_management.php");
    }
}

if (isset($_POST['update'])) {
    $product = Product::find_by_id($_POST['id']);
    $product->name = trim($_POST['name']);//required
    $product->code = trim($_POST['code']);//required
    $product->category_id = trim($_POST['category_id']);//required
    $product->roq = trim($_POST['roq']);//required
    $product->max_qty = trim($_POST['max_qty']);//required
    $product->min_qty = trim($_POST['min_qty']);//required

    //new fields 
    $product->barcode = trim($_POST['barcode']);//required
    $product->lot_number = trim($_POST['lot_number']);
    $product->description = trim($_POST['description']);
    $product->exp_date_time = trim($_POST['exp_date_time']);
    $product->manufacturer = trim($_POST['manufacturer']);
    $product->contract_period = trim($_POST['contract_period']);
    $product->department_id = trim((isset($_POST['department_id'])) ? $_POST['department_id'] : '');
    $product->location = trim($_POST['location']);
    $product->shelf_number = trim($_POST['shelf_number']);

    try {
        $product->save();
        Activity::log_action("Product - updated : ".$product->name);
        $_SESSION["message"] = "Successfully updated.";
        Functions::redirect_to("./../product.php");
        Functions::redirect_to("./../product_management.php");
    } catch (Exception $exc) {
        $_SESSION["error"] = "Error..! Failed to update.";
        Functions::redirect_to("./../product.php");
        Functions::redirect_to("../product_management.php");
    }
}


if (isset($_POST['delete'])) {
    $product = Product::find_by_id($_POST["id"]);
    
    try {
        $product->delete();
        Activity::log_action("Product - deleted : ".$product->name);
        $_SESSION["message"] = "Successfully deleted.";
        Functions::redirect_to("./../product_management.php");
//        Functions::redirect_to("./../product_management.php");
    } catch (Exception $exc) {
        $_SESSION["error"] = "Error..! Failed to delete.";
        Functions::redirect_to("./../product_management.php");
//        Functions::redirect_to("./../product_management.php");
    }
}
?>