<?php

class ProjectConfig {

    public static $project_name = "INVENTORY PROJECT";
    public static $address = "Address";
    public static $tel = "Phone: - / -";
    public static $address_arr = array("INVENTORY PROJECT", "Address", "Address", "Address", "Address");
    public static $tel_arr = array("-", "-");
    public static $address_html = "<strong>INVENTORY PROJECT</strong>,<br/>Address,<br/>Address,<br/>Address,<br/>Address.";
    public static $tel_html = "<strong>Phone:</strong> - / -";

}
